//
//  DetalhesViewController.swift
//  Lista_Contatos
//
//  Created by COTEMIG on 05/02/1444 AH.
//

import UIKit

protocol  DetalhesViewControllerDelegate {
    func excluirContato(index: Int)
}

class DetalhesViewController: UIViewController {
    
    
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var numeroLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var enderecoLabel: UILabel!
    
    public var index: Int?
    public var contato: Contato?
    public var delegate: DetalhesViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = contato?.nome

        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.numero
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
        
    }
    
    @IBAction func excluirContato(_ sender: Any) {
        delegate?.excluirContato(index: index!)
        navigationController?.popViewController(animated: true)
    }
    

}

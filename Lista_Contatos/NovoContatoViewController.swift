//
//  NovoContatoViewController.swift
//  Lista_Contatos
//
//  Created by COTEMIG on 05/02/1444 AH.
//

import UIKit

protocol NovoContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato)
}

class NovoContatoViewController: UIViewController {
    
    @IBOutlet weak var nomeField: UITextField!
    @IBOutlet weak var numeroField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var enderecoField: UITextField!
    
    public var delegate: NovoContatoViewControllerDelegate?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func salvarContato(_ sender: Any) {
        let contato = Contato(nome: nomeField?.text ?? "", email: emailField?.text ?? "", endereco: enderecoField?.text ?? "", numero: numeroField?.text ?? "")
        delegate?.salvarNovoContato(contato: contato)
        navigationController?.popViewController(animated: true)
    }
    
    
    

}

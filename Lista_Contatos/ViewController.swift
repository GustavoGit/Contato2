//
//  ViewController.swift
//  Lista_Contatos
//
//  Created by COTEMIG on 14/01/1444 AH.
//

import UIKit

struct Contato {
    let nome : String
    let email : String
    let endereco : String
    let numero : String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.endereco.text = contato.endereco
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        
        return cell
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.dataSource = self
        tableView.delegate = self
        
        
        listaDeContatos.append(Contato(nome: "Contato 1", email: "31 98989-000", endereco:
                                        "contato1@email.com.br", numero: "Rua do Cristal, 11"))
        listaDeContatos.append(Contato(nome: "Contato 2", email: "31 98989-111", endereco:
                                        "contato2@email.com.br", numero: "Rua do Cristal, 12"))
        listaDeContatos.append(Contato(nome: "Contato 3", email: "31 98989-222", endereco:
                                        "contato3@email.com.br", numero: "Rua do Cristal, 13"))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirDetalhes" {
            let detalhesViewController = segue.destination as! DetalhesViewController
            let index = sender as! Int
            let contato = listaDeContatos[index]
            detalhesViewController.index = index
            detalhesViewController.contato = contato
            detalhesViewController.delegate = self
        } else if segue.identifier == "criarContato" {
            let novoContatoViewController = segue.destination as! NovoContatoViewController
            novoContatoViewController.delegate = self
        }
    }
    
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
    }
}

extension ViewController: NovoContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato) {
        listaDeContatos.append(contato)
        tableView.reloadData()
    }
}

extension ViewController: DetalhesViewControllerDelegate {
    func excluirContato(index: Int) {
        listaDeContatos.remove(at: index)
        tableView.reloadData()
    }
}
